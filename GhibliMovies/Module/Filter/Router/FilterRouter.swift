//
//  FilterRouter.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 11/03/21.
//

import UIKit

protocol FilterRouterProtocol {
    func route(to route: FilterRouter.Route)
}

final class FilterRouter {

    enum Route {
        case home
    }

    // MARK: - Properties

    private var context: UIViewController? = nil

    // MARK: - Methods

    func setContext(context: UIViewController) {
        self.context = context
    }

    private func dismissFilterViewController() {
        context?.dismiss(animated: true, completion: nil)
    }

}

// MARK: - HomeRouterProtocol

extension FilterRouter: FilterRouterProtocol {

    func route(to route: Route) {
        switch route {
        case .home: dismissFilterViewController()
        }
    }

}
