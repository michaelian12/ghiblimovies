//
//  FilterPresenter.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import Foundation
import RxCocoa

protocol FilterPresenterProtocol: AnyObject {
    var releaseYears: [String] { get set }
    var selectedReleaseYear: BehaviorRelay<String> { get set }
    func dismissFilterViewController()
}

final class FilterPresenter {

    // MARK: - Properties

    private let filterRouter: FilterRouterProtocol
    var releaseYears: [String]
    var selectedReleaseYear: BehaviorRelay<String>

    // MARK: - Initialisation

    init(releaseYears: [String], selectedReleaseYear: BehaviorRelay<String>, filterRouter: FilterRouterProtocol) {
        self.releaseYears = releaseYears
        self.selectedReleaseYear = selectedReleaseYear
        self.filterRouter = filterRouter
    }

}

// MARK: - FilterPresenterProtocol

extension FilterPresenter: FilterPresenterProtocol {

    func dismissFilterViewController() {
        filterRouter.route(to: .home)
    }

}
