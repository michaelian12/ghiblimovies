//
//  FilterViewController.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import UIKit

final class FilterViewController: UIViewController {

    // MARK: - UI Properties

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Select Release Year"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var releaseYearPicker: UIPickerView = {
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()

    private lazy var applyFilterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Apply Filter", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 16
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(applyFilterButtonDidTap), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    private lazy var resetFilterButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Reset Filter", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.systemBlue, for: .normal)
        button.addTarget(self, action: #selector(resetFilterButtonDidTap), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: - Properties

    private let presenter: FilterPresenterProtocol

    // MARK: - Initialisation

    init(presenter: FilterPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    // MARK: - Methods

    private func setupViews() {
        view.backgroundColor = .systemBackground
        view.addSubview(titleLabel)
        view.addSubview(releaseYearPicker)
        view.addSubview(applyFilterButton)
        view.addSubview(resetFilterButton)

        NSLayoutConstraint.activate([
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: releaseYearPicker.topAnchor, constant: -16),
            releaseYearPicker.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            releaseYearPicker.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            releaseYearPicker.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            applyFilterButton.heightAnchor.constraint(equalToConstant: 48),
            applyFilterButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            applyFilterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            resetFilterButton.topAnchor.constraint(equalTo: applyFilterButton.bottomAnchor, constant: 16),
            resetFilterButton.heightAnchor.constraint(equalToConstant: 48),
            resetFilterButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            resetFilterButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            resetFilterButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
        ])
    }

    // MARK: - Action Methods

    @objc private func applyFilterButtonDidTap() {
        let selectedIndex = self.releaseYearPicker.selectedRow(inComponent: 0)
        presenter.selectedReleaseYear.accept(self.presenter.releaseYears[selectedIndex])
        presenter.dismissFilterViewController()
    }

    @objc private func resetFilterButtonDidTap() {
        presenter.selectedReleaseYear.accept("")
        presenter.dismissFilterViewController()
    }

}

// MARK: - UIPickerViewDelegate

extension FilterViewController: UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.releaseYears[row].description
    }

}

// MARK: - UIPickerViewDataSource

extension FilterViewController: UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return presenter.releaseYears.count
    }

}

