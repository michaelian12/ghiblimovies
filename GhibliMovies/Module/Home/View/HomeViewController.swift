//
//  HomeViewController.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import UIKit
import RxSwift
import RxCocoa

final class HomeViewController: UIViewController {

    // MARK: - UI Properties

    private lazy var filterButton: UIBarButtonItem = {
        let button = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filterButtonDidTap))
        return button
    }()

    private lazy var movieTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .systemBackground
        tableView.allowsSelection = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    private lazy var activityIndicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        return indicator
    }()

    // MARK: - Properties

    private let presenter: HomePresenterProtocol
    private let disposeBag = DisposeBag()

    // MARK: - Initialisation

    init(presenter: HomePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupViews()
        setupTableView()
        setupBinding()
        presenter.getMovies(withReleaseYear: nil)
    }

    // MARK: - Methods

    private func setupNavigation() {
        navigationItem.title = "Movies"
        navigationItem.rightBarButtonItem = filterButton
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .always
    }

    private func setupViews() {
        view.addSubview(movieTableView)
        view.addSubview(activityIndicatorView)
        movieTableView.fillSuperview()
        activityIndicatorView.fillSuperview()
    }

    private func setupTableView() {
        movieTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: MovieTableViewCell.className())
    }

    private func setupBinding() {
        presenter.selectedReleaseYear
            .subscribe { value in
                self.filterButton.setTitleTextAttributes([.font: value.isEmpty ?
                                                            UIFont.systemFont(ofSize: 16) :
                                                            UIFont.boldSystemFont(ofSize: 16)],
                                                         for: .normal)
            }
            .disposed(by: disposeBag)

        presenter.movies
            .bind(to: movieTableView.rx.items(cellIdentifier: MovieTableViewCell.className(), cellType: MovieTableViewCell.self)) { row, model, cell in
                cell.setMovie(model)
            }
            .disposed(by: disposeBag)

        presenter.isLoading
            .bind(to: activityIndicatorView.rx.isAnimating)
            .disposed(by: disposeBag)
    }

    // MARK: - Action Method

    @objc private func filterButtonDidTap() {
        presenter.presentFilterViewController()
    }

}
