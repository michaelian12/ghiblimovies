//
//  MovieTableViewCell.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import UIKit

final class MovieTableViewCell: UITableViewCell {

    // MARK: - UI Properties

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title"
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Description"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var releaseYearLabel: UILabel = {
        let label = UILabel()
        label.text = "Release"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    private lazy var directorLabel: UILabel = {
        let label = UILabel()
        label.text = "Director"
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    // MARK: - Properties

    private var movie: MovieModel? = nil

    // MARK: - Initialisation

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func setupViews() {
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(releaseYearLabel)
        addSubview(directorLabel)

        addConstraintsWithFormat("H:|-16-[v0]-16-|", views: titleLabel)
        addConstraintsWithFormat("H:|-16-[v0]-16-|", views: descriptionLabel)
        addConstraintsWithFormat("H:|-16-[v0]-16-|", views: releaseYearLabel)
        addConstraintsWithFormat("H:|-16-[v0]-16-|", views: directorLabel)
        addConstraintsWithFormat("V:|-16-[v0]-[v1]-[v2]-[v3]-16-|", views: titleLabel, descriptionLabel, releaseYearLabel, directorLabel)
    }

    func setMovie(_ movie: MovieModel) {
        self.movie = movie
        titleLabel.text = movie.title
        descriptionLabel.text = movie.description
        releaseYearLabel.text = movie.releaseYear
        directorLabel.text = movie.director
    }
    
}
