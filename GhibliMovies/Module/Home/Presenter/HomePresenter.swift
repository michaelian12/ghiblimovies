//
//  HomePresenter.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import UIKit
import RxSwift
import RxCocoa

protocol HomePresenterProtocol: AnyObject {
    var movies: BehaviorRelay<[MovieModel]> { get set }
    var selectedReleaseYear: BehaviorRelay<String> { get set }
    var isLoading: BehaviorRelay<Bool> { get set }
    var errorMessage: BehaviorRelay<String> { get set }
    func getMovies(withReleaseYear year: String?)
    func presentFilterViewController()
}

final class HomePresenter {

    // MARK: - Properties

    private let homeUseCase: HomeUseCase
    private let homeRouter: HomeRouterProtocol
    private let disposeBag = DisposeBag()
    private var releaseYears = [String]()

    var movies: BehaviorRelay<[MovieModel]>
    var selectedReleaseYear: BehaviorRelay<String>
    var isLoading: BehaviorRelay<Bool>
    var errorMessage: BehaviorRelay<String>

    // MARK: - Initialisation

    init(homeUseCase: HomeUseCase, homeRouter: HomeRouterProtocol,
         movies: BehaviorRelay<[MovieModel]>, selectedReleaseYear: BehaviorRelay<String>,
         isLoading: BehaviorRelay<Bool>, errorMessage: BehaviorRelay<String>) {
        self.homeUseCase = homeUseCase
        self.homeRouter = homeRouter
        self.movies = movies
        self.selectedReleaseYear = selectedReleaseYear
        self.isLoading = isLoading
        self.errorMessage = errorMessage
        setupBinding()
    }

    // MARK: - Methods

    private func setupBinding() {
        selectedReleaseYear
            .skip(1)
            .subscribe { value in
                self.getMovies(withReleaseYear: value)
            }
            .disposed(by: disposeBag)
    }

    private func getUniqueAndSortedReleaseYear(from movies: [MovieModel]) -> [String] {
        let releaseYears = movies.map { (Int($0.releaseYear) ?? 0) }
        let uniqueReleaseYears = Set(releaseYears)
        let sortedReleaseYears = uniqueReleaseYears.sorted { $0 > $1 }
        return sortedReleaseYears.map { String($0) }
    }

}

// MARK: - HomePresenterProtocol

extension HomePresenter: HomePresenterProtocol {

    func getMovies(withReleaseYear year: String?) {
        movies.accept([])
        isLoading.accept(true)

        homeUseCase.getMovies(withReleaseYear: year)
            .observe(on: MainScheduler.instance)
            .subscribe { result in
                self.movies.accept(result)
                guard let year = year, !year.isEmpty else {
                    self.releaseYears = self.getUniqueAndSortedReleaseYear(from: result)
                    return
                }
            } onError: { error in
                self.errorMessage.accept(error.localizedDescription)
            } onCompleted: {
                self.isLoading.accept(false)
            }.disposed(by: disposeBag)
    }

    func presentFilterViewController() {
        homeRouter.route(to: .filter(releaseYears: releaseYears, selectedReleaseYear: selectedReleaseYear))
    }

}
