//
//  HomeRouter.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import UIKit
import RxCocoa

protocol HomeRouterProtocol {
    func route(to route: HomeRouter.Route)
}

final class HomeRouter {

    enum Route {
        case filter(releaseYears: [String], selectedReleaseYear: BehaviorRelay<String>)
    }

    // MARK: - Properties

    private var context: UIViewController? = nil

    // MARK: - Methods

    func setContext(context: UIViewController) {
        self.context = context
    }

    private func presentFilterViewController(releaseYears: [String], selectedReleaseYear: BehaviorRelay<String>) {
        let filterRouter = FilterRouter()
        let filterPresenter = FilterPresenter(releaseYears: releaseYears, selectedReleaseYear: selectedReleaseYear, filterRouter: filterRouter)
        let filterViewController = FilterViewController(presenter: filterPresenter)
        filterRouter.setContext(context: filterViewController)
        context?.present(filterViewController, animated: true, completion: nil)
    }

}

// MARK: - HomeRouterProtocol

extension HomeRouter: HomeRouterProtocol {

    func route(to route: Route) {
        switch route {
        case .filter(let releaseYears, let selectedReleaseYear):
            presentFilterViewController(releaseYears: releaseYears, selectedReleaseYear: selectedReleaseYear)
        }
    }

}
