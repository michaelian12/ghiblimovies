//
//  CustomError+Extension.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import Foundation

enum URLError: LocalizedError {

    case invalidResponse

    var errorDescription: String? {
        switch self {
        case .invalidResponse: return "The server responded with garbage."
        }
    }

}

enum DatabaseError: LocalizedError {

    case invalidInstance
    case requestFailed

    var errorDescription: String? {
        switch self {
        case .invalidInstance: return "Database can't instance."
        case .requestFailed: return "Your request failed."
        }
    }

}
