//
//  MovieMapper.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation

final class MovieMapper {

    static func mapMovieResponsesToDomains(input movieResponse: [MovieResponse]) -> [MovieModel] {
        return movieResponse.map { result in
            return MovieModel(id: result.id ?? "",
                              title: result.title ?? "Unknown title",
                              description: result.description ?? "Unknown description",
                              releaseYear: result.releaseYear ?? "Unknown release year",
                              director: result.director ?? "Unknown director")
        }
    }

    static func mapMovieResponsesToEntities(input movieResponses: [MovieResponse]) -> [MovieEntity] {
        return movieResponses.map { result in
            let newMovie = MovieEntity()
            newMovie.id = result.id ?? ""
            newMovie.title = result.title ?? "Unknown title"
            newMovie.desc = result.description ?? "Unknown description"
            newMovie.releaseYear = result.releaseYear ?? "Unknown release year"
            newMovie.director = result.director ?? "Unknown director"
            return newMovie
        }
    }

    static func mapMovieEntitiesToDomains(input movieEntities: [MovieEntity]) -> [MovieModel] {
        return movieEntities.map { result in
            return MovieModel(
                id: result.id,
                title: result.title,
                description: result.desc,
                releaseYear: result.releaseYear,
                director: result.director
            )
        }
    }

}
