//
//  APICall.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation

struct API {
    static let baseUrl = "https://ghibliapi.herokuapp.com/"
}

protocol Endpoint {
    var url: String { get }
}

enum Endpoints {

    enum Gets: Endpoint {
        case movies

        var url: String {
            switch self {
            case .movies: return "\(API.baseUrl)films"
            }
        }
    }
    
}
