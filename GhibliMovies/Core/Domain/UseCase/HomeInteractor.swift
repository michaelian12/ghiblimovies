//
//  HomeInteractor.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation
import RxSwift

protocol HomeUseCase {
    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]>
}

final class HomeInteractor: HomeUseCase {

    private let repository: MovieRepositoryProtocol

    init(repository: MovieRepositoryProtocol) {
        self.repository = repository
    }

    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]> {
        guard let year = year, !year.isEmpty else {
            return repository.getMovies(withReleaseYear: nil)
        }

        return repository.getMovies(withReleaseYear: year)
            .map {
                $0.filter { $0.releaseYear == year }
            }
    }

}
