//
//  MovieModel.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation

struct MovieModel: Equatable {

    let id: String
    let title: String
    let description: String
    let releaseYear: String
    let director: String

}
