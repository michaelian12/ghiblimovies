//
//  MovieRepository.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation
import RxSwift

protocol MovieRepositoryProtocol {
    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]>
}

final class MovieRepository {

    typealias MovieInstance = (RemoteDataSource, LocalDataSource) -> MovieRepository

    fileprivate let remote: RemoteDataSource
    fileprivate let local: LocalDataSource

    private init(remote: RemoteDataSource, local: LocalDataSource) {
        self.remote = remote
        self.local = local
    }

    static let sharedInstance: MovieInstance = { remoteRepo, localRepo in
        return MovieRepository(remote: remoteRepo, local: localRepo)
    }
}

// MARK: - MovieRepositoryProtocol

extension MovieRepository: MovieRepositoryProtocol {

    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]> {
        return local.getMovies(withReleaseYear: year)
            .map { MovieMapper.mapMovieEntitiesToDomains(input: $0) }
            .filter { !$0.isEmpty }
            .ifEmpty(switchTo: remote.getMovies()
                        .map { MovieMapper.mapMovieResponsesToEntities(input: $0) }
                        .flatMap { self.local.addMovies($0) }
                        .filter { $0 }
                        .flatMap { _ in
                            self.local.getMovies(withReleaseYear: year)
                                .map { MovieMapper.mapMovieEntitiesToDomains(input: $0) }
                        }
            )
    }

}
