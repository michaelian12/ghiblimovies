//
//  LocalDataSource.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import Foundation
import RealmSwift
import RxSwift

protocol LocalDataSourceProtocol: AnyObject {
    func getMovies(withReleaseYear year: String?) -> Observable<[MovieEntity]>
    func addMovies(_ movies: [MovieEntity]) -> Observable<Bool>
}

final class LocalDataSource: NSObject {

    private let realm: Realm?

    private init(realm: Realm?) {
        self.realm = realm
    }

    static let sharedInstance: (Realm?) -> LocalDataSource = { realm in
        return LocalDataSource(realm: realm)
    }

}

// MARK: - LocalDataSourceProtocol

extension LocalDataSource: LocalDataSourceProtocol {

    func getMovies(withReleaseYear year: String?) -> Observable<[MovieEntity]> {
        return Observable<[MovieEntity]>.create { observer in
            if let realm = self.realm {
                let movies: Results<MovieEntity> = {
                    realm.objects(MovieEntity.self)
                }()

                observer.onNext(movies.toArray(ofType: MovieEntity.self))
                observer.onCompleted()
            } else {
                observer.onError(DatabaseError.invalidInstance)
            }

            return Disposables.create()
        }
    }

    func addMovies(_ movies: [MovieEntity]) -> Observable<Bool> {
        return Observable<Bool>.create { observer in
            if let realm = self.realm {
                do {
                    try realm.write {
                        for movie in movies {
                            realm.add(movie, update: .all)
                        }
                        observer.onNext(true)
                        observer.onCompleted()
                    }
                } catch {
                    observer.onError(DatabaseError.requestFailed)
                }
            } else {
                observer.onError(DatabaseError.invalidInstance)
            }

            return Disposables.create()
        }
    }

}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for index in 0 ..< count {
            if let result = self[index] as? T {
                array.append(result)
            }
        }
        return array
    }
}
