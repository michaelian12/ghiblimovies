//
//  MovieEntity.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import Foundation
import RealmSwift

class MovieEntity: Object {

    @objc dynamic var id: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var desc: String = ""
    @objc dynamic var releaseYear: String = ""
    @objc dynamic var director: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }

}
