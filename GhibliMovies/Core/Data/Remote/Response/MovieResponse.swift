//
//  MovieResponse.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation

struct MovieResponse: Decodable {

    private enum CodingKeys: String, CodingKey {
        case id, title, description, director
        case releaseYear = "release_date"
    }

    let id: String?
    let title: String?
    let description: String?
    let releaseYear: String?
    let director: String?

}
