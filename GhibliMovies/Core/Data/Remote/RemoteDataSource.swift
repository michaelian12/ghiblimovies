//
//  RemoteDataSource.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 09/03/21.
//

import Foundation
import Alamofire
import RxSwift

protocol RemoteDataSourceProtocol: AnyObject {
    func getMovies() -> Observable<[MovieResponse]>
}

final class RemoteDataSource: NSObject {

    private override init() { }

    static let sharedInstance: RemoteDataSource = RemoteDataSource()

}

// MARK: - RemoteDataSourceProtocol

extension RemoteDataSource: RemoteDataSourceProtocol {
    func getMovies() -> Observable<[MovieResponse]> {
        return Observable<[MovieResponse]>.create { observer in
            if let url = URL(string: Endpoints.Gets.movies.url) {
                AF.request(url).validate().responseDecodable(of: [MovieResponse].self) { response in
                    switch response.result {
                    case .success(let movies):
                        observer.onNext(movies)
                        observer.onCompleted()
                    case .failure:
                        observer.onError(URLError.invalidResponse)
                    }
                }
            }

            return Disposables.create()
        }
    }
}
