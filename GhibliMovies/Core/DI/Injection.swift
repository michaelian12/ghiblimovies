//
//  Injection.swift
//  GhibliMovies
//
//  Created by Michael Agustian on 10/03/21.
//

import Foundation
import RealmSwift

final class Injection {

    private func provideRepository() -> MovieRepository {
        let realm = try? Realm()
        let local: LocalDataSource = LocalDataSource.sharedInstance(realm)
        let remote: RemoteDataSource = RemoteDataSource.sharedInstance
        return MovieRepository.sharedInstance(remote, local)
    }

    func provideHome() -> HomeUseCase {
        let repository = provideRepository()
        return HomeInteractor(repository: repository)
    }

}
