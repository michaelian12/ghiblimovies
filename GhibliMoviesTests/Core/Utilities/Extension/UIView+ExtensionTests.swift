//
//  UIView+ExtensionTests.swift
//  GhibliMoviesTests
//
//  Created by Michael Agustian on 13/03/21.
//

import XCTest
@testable import GhibliMovies

class UIView_ExtensionTests: XCTestCase {

    func classNameTest() throws {
        // Given
        let movieTableViewCell = MovieTableViewCell.self

        // When
        let movieTableViewCellClassName = movieTableViewCell.className()

        // Then
        XCTAssertEqual(movieTableViewCellClassName, "MovieTableViewCell")
    }

}
