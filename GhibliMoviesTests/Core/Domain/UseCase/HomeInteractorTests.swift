//
//  HomeInteractorTests.swift
//  GhibliMoviesTests
//
//  Created by Michael Agustian on 12/03/21.
//

import XCTest
import RxSwift
import RxBlocking
@testable import GhibliMovies

class HomeInteractorTests: XCTestCase {

    private var releaseYear: String!
    private var movieRepository: MovieRepositoryMock!
    private var homeInteractor: HomeInteractor!
    private var movie1: MovieModel!
    private var movie2: MovieModel!
    private var movie3: MovieModel!

    override func setUpWithError() throws {
        releaseYear = nil
        movieRepository = MovieRepositoryMock()
        homeInteractor = HomeInteractor(repository: movieRepository)
        movie1 = MovieModel(id: "1", title: "title1", description: "description1", releaseYear: "2000", director: "director1")
        movie2 = MovieModel(id: "2", title: "title2", description: "description2", releaseYear: "2001", director: "director2")
        movie3 = MovieModel(id: "3", title: "title3", description: "description3", releaseYear: "2002", director: "director3")
    }

    override func tearDownWithError() throws {
        releaseYear = nil
        movieRepository = nil
        homeInteractor = nil
        movie1 = nil
        movie2 = nil
        movie3 = nil
    }

    func testGetMovies_returnAllMovies_withReleaseYearIsNil() throws {
        // Given
        releaseYear = nil
        movieRepository.moviesReturnValue = .just([movie1, movie2, movie3])

        // When
        let movies = homeInteractor.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(try movies.toBlocking().first(), [movie1, movie2, movie3])
    }

    func testGetMovies_returnAllMovies_withReleaseYearIsEmpty() throws {
        // Given
        releaseYear = ""
        movieRepository.moviesReturnValue = .just([movie1, movie2, movie3])

        // When
        let movies = homeInteractor.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(try movies.toBlocking().first(), [movie1, movie2, movie3])
    }

    func testGetMovies_returnOneMovie() throws {
        // Given
        releaseYear = "2000"
        movieRepository.moviesReturnValue = .just([movie1, movie2, movie3])

        // When
        let movies = homeInteractor.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(try movies.toBlocking().first(), [movie1])
    }

    func testGetMovies_returnEmptyMovie() throws {
        // Given
        releaseYear = "2003"
        movieRepository.moviesReturnValue = .just([movie1, movie2, movie3])

        // When
        let movies = homeInteractor.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(try movies.toBlocking().first(), [])
    }

    func testGetMovies_returnFailed() throws {
        // Given
        releaseYear = "2003"
        movieRepository.moviesReturnValue = .error(URLError.invalidResponse)

        // When
        let movies = homeInteractor.getMovies(withReleaseYear: releaseYear)

        // Then
        let result = movies.toBlocking().materialize()

        switch result {
        case .completed:
            XCTFail("Expected result to complete with error, but result was successful.")
        case .failed(_, let error):
            XCTAssertEqual(error.localizedDescription, URLError.invalidResponse.localizedDescription)
        }
    }

}

class MovieRepositoryMock: MovieRepositoryProtocol {

    var moviesReturnValue: Observable<[MovieModel]>!

    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]> {
        return moviesReturnValue
    }

}
