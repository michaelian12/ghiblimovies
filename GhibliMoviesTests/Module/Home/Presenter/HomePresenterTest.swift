//
//  HomePresenterTest.swift
//  GhibliMoviesTests
//
//  Created by Michael Agustian on 13/03/21.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
@testable import GhibliMovies

class HomePresenterTest: XCTestCase {

    private var releaseYear: String!
    private var homeInteractor: HomeInteractorMock!
    private var homeRouter: HomeRouterMock!
    private var homePresenter: HomePresenter!
    private var disposeBag: DisposeBag!
    private var movie1: MovieModel!
    private var movie2: MovieModel!
    private var movie3: MovieModel!

    override func setUpWithError() throws {
        releaseYear = nil
        homeInteractor = HomeInteractorMock()
        homeRouter = HomeRouterMock()
        homePresenter = HomePresenter(homeUseCase: homeInteractor, homeRouter: homeRouter,
                                      movies: BehaviorRelay<[MovieModel]>(value: []), selectedReleaseYear: BehaviorRelay<String>(value: ""),
                                      isLoading: BehaviorRelay<Bool>(value: false), errorMessage: BehaviorRelay<String>(value: ""))
        disposeBag = DisposeBag()
        movie1 = MovieModel(id: "1", title: "title1", description: "description1", releaseYear: "2000", director: "director1")
        movie2 = MovieModel(id: "2", title: "title2", description: "description2", releaseYear: "2001", director: "director2")
        movie3 = MovieModel(id: "3", title: "title3", description: "description3", releaseYear: "2002", director: "director3")
    }

    override func tearDownWithError() throws {
        releaseYear = nil
        homeInteractor = nil
        homeRouter = nil
        homePresenter = nil
        disposeBag = nil
        movie1 = nil
        movie2 = nil
        movie3 = nil
    }

    func testGetMovies_returnAllMovies() throws {
        // Given
        releaseYear = nil
        homeInteractor.moviesReturnValue = .just([movie1, movie2, movie3])

        // When
        homePresenter.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(homePresenter.movies.value, [movie1, movie2, movie3])
        XCTAssertEqual(homePresenter.errorMessage.value, "")
    }

    func testGetMovies_returnError() throws {
        // Given
        releaseYear = nil
        homeInteractor.moviesReturnValue = .error(URLError.invalidResponse)

        // When
        homePresenter.getMovies(withReleaseYear: releaseYear)

        // Then
        XCTAssertEqual(homePresenter.movies.value, [])
        XCTAssertEqual(homePresenter.errorMessage.value, URLError.invalidResponse.localizedDescription)
    }

}

class HomeInteractorMock: HomeUseCase {

    var moviesReturnValue: Observable<[MovieModel]>!

    func getMovies(withReleaseYear year: String?) -> Observable<[MovieModel]> {
        return moviesReturnValue
    }
    
}

class HomeRouterMock: HomeRouterProtocol {
    func route(to route: HomeRouter.Route) {
    }
}
